/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.reportproject;

import java.util.List;

/**
 *
 * @author ASUS
 */
public class ArtistService {
    public List<ArtistReport> getTopTenArtistByToyalPrice(){
        ArtistDao artistDao = new ArtistDao();
        return artistDao.getArtistByToyalPrice(10);
    }
    public List<ArtistReport> getTopTenArtistByToyalPrice(String begin,String end){
        ArtistDao artistDao = new ArtistDao();
        return artistDao.getArtistByToyalPrice(begin, end, 10);
    }
}
